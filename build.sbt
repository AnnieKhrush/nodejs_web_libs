import org.scalajs.linker.interface.ModuleInitializer

name := "Scala.js Application"
scalaVersion := "2.13.8"

// project.enablePlugins(ScalablyTypedConverterPlugin)
enablePlugins(ScalaJSPlugin)
//enablePlugins(ScalaJSBundlerPlugin)
scalaJSUseMainModuleInitializer := true

val http4sVersion = "0.23.7"

// resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies ++= Seq(
  "org.http4s" %%% "http4s-ember-client" % http4sVersion,
  "org.http4s" %%% "http4s-ember-server" % http4sVersion,
  "org.http4s" %%% "http4s-dsl"          % http4sVersion,
  "ch.qos.logback" % "logback-classic"      % "1.2.3"
)

//for s3
libraryDependencies += "net.exoego" %%% "aws-sdk-scalajs-facade-s3" % "0.33.0-v2.892.0"
//npmDependencies in Compile += "aws-sdk" -> "2.892.0"

scalaJSLinkerConfig ~= { _.withModuleKind(ModuleKind.ESModule) }
// CommonJS
scalaJSLinkerConfig ~= { _.withModuleKind(ModuleKind.CommonJSModule) }