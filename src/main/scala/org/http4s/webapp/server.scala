package org.http4s.webapp

import cats.effect._
import com.comcast.ip4s._
import org.http4s.dsl.io._
import org.http4s.implicits._
import org.http4s.ember.server._

/*
object Server extends IOApp {

  def run(args: List[String]): IO[ExitCode] =
    EmberServerBuilder
      .default[IO]
      .withHost(ipv4"0.0.0.0")
      .withPort(port"8080")
      .withHttpApp(TestApp.helloWorldService)
      .build
      .use(_ => IO.never)
      .as(ExitCode.Success)
}
*/