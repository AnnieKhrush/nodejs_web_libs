package facade.amazonaws.services.s3

import facade.amazonaws.AWSConfig
import facade.amazonaws.AWS

object TestBucket extends App {
  val service = new S3(AWSConfig(endpoint = "http://127.0.0.1/"))

  service.getObjectFuture(GetObjectRequest(
    Bucket = "testbucket",
    Key = "test"
  ))
}
