package org.http4s.webapp

import cats.effect._
import org.http4s._
import org.http4s.dsl.io._
import org.http4s.HttpRoutes

object TestApp {
  val helloWorldService = HttpRoutes.of[IO] {
    case GET -> Root / "hello" / name =>
      Ok(s"Hello, $name.")
  }.orNotFound
}

